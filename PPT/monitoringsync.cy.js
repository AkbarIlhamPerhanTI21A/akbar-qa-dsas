/// <reference types="cypress"/>

describe (" Monitoring Sync Product EBIS", ()=> {


    beforeEach(() => {
        cy.visit('https://ppt-dev.apps.mypaas.telkom.co.id/login'); // Mengunjungi halaman utama EBIS
        cy.get(':nth-child(3) > .controls > .textinput').type('user_test_cypress'); //Login Dengan Positif User
        cy.get(':nth-child(4) > .controls > .textinput').type('Neuron123'); //positif password
        cy.get('#form-login-view > .login100-form > .btn').click(); 
      })


    it ('Monitoring Sync Product', ()=> {
        
        cy.url().should('include', '/dashboard') // Memastikan bahwa sudah diarahkan ke dashboard setelah login

        //Masuk ke EBIS
        cy.get('.dropdown > .text-light').click();
        cy.contains('Go To EBIS').click();
        cy.url().should('include', '/ebis/dashboard'); // memastikan bahwa sudah diarahkan ke ebis/dashboard
    
        //masuk ke monitoring
        cy.contains('Monitoring').click();

        //masuk ke tasks synchronization
        cy.contains('Tasks Synchronization').click()
        cy.url().should('include', '/ebis/monitoring/tasks/sync');// memastikan bahwa sudah diarakhan ke halaman task/sync

        //detail sync product
        cy.get(':nth-child(1) > :nth-child(9) > .btn > .fa').click(); //pilih order
        cy.get('.modal-content').should('be.visible'); // memastikan bahwa muncul pop up setelah order
        cy.contains('Close').click();

        cy.get(':nth-child(3) > :nth-child(9) > .btn > .fa').click(); //mencoba lagi dan memilih order lain
        cy.contains('Close').click();

        //logout
        cy.get('.user-profile').click();
        cy.get('#logout-form > a').click();

        
    });

});